# Manually modifying the style types 
set style line  1 lc 1 lw 3 ps 2 dt 1
set style line  2 lc 2 lw 3 ps 2 dt 2
set style line  3 lc 3 lw 3 ps 2 dt 3
set style line  4 lc 4 lw 3 ps 2 dt 4
set style line  5 lc 5 lw 3 ps 2 dt 5
set style line  6 lc 6 lw 3 ps 2 dt 6
set style line  7 lc 7 lw 3 ps 2 dt 7
set style line  8 lc 8 lw 3 ps 2 dt 8
set style line  9 lc 9 lw 3 ps 2 dt 9
set style line  10 lc 10 lw 3 ps 2 dt 12
set style line  11 lc 11 lw 3 ps 2 dt 13
set style line  12 lc 12 lw 3 ps 2 dt 14
set style line  13 lc 13 lw 3 ps 2 dt 15
set style line  14 lc 14 lw 3 ps 2 dt 16
set style line  15 lc 15 lw 3 ps 2 dt 17
set style line  16 lc 16 lw 3 ps 2 dt 18
set style line  17 lc 17 lw 3 ps 2 dt 19
set style line  18 lc 18 lw 3 ps 2 dt 20
set style line  19 lc 19 lw 3 ps 2 dt 21
set style line  41 lc 1 lw 3 ps 2 dt 1            
set style line  42 lc 2 lw 3 ps 2 dt 1            
set style line  43 lc 3 lw 3 ps 2 dt 1            
set style line  44 lc 4 lw 3 ps 2 dt 1    
  
set terminal postscript eps enhanced colour size 8,12 font 'Times-Roman,20' 
set output 'fig1a.eps' 
  
m    = 2.310*1.6726219e-24
mar  = 10
nmax = 1.0e24
set multiplot layout 4,2
set log x 
set log x2
set xr  [1e2  :1e22  ]
set x2r [1e2*m:1e22*m] 
set format x  ""
set format x2 "%L"
set x2l "log {/Symbol r}_n (g cm^{-3})"
set x2tics
set xtics nomirror

set key top left
unset key
set tmargin 4
set bmargin 0
set lmargin mar
set rmargin 0

#Plot number densities 
unset label 1
set lmargin mar
set rmargin 0
set log y 
set yl 'log n (cm^{-3})' 
set yr [1e-11:nmax] 
set format y ' %3L' 
set key top left
plot '../../data/eta_density.dat' u 11:14 ti ''     w l ls  9 lw 8,\
     '../../data/eta_density.dat' u 11:15 ti 'H_3+' w l ls  2 ,\
     '../../data/eta_density.dat' u 11:19 ti 'He+'  w l ls  3 ,\
     '../../data/eta_density.dat' u 11:18 ti 'H+'   w l ls  4 ,\
     '../../data/eta_density.dat' u 11:20 ti 'C+'   w l ls  6 ,\
     '../../data/eta_density.dat' u 11:21 ti 'O+'   w l ls  7 ,\
     '../../data/eta_density.dat' u 11:17 ti 'O_2+' w l ls  8 ,\
     '../../data/eta_density.dat' u 11:24 ti 'Mg+'  w l ls  9 ,\
     '../../data/eta_density.dat' u 11:22 ti 'Si+'  w l ls 15 ,\
     '../../data/eta_density.dat' u 11:23 ti 'S+'   w l ls 16 ,\
     '../../data/eta_density.dat' u 11:16 ti 'HCO+' w l ls 10 ,\
     '../../data/eta_density.dat' u 11:25 ti ''     w l ls 12 ,\
     '../../data/eta_density.dat' u 11:26 ti ''     w l ls 13 ,\
     '../../data/eta_density.dat' u 11:27 ti ''     w l ls 14 ,\
     '../../data/eta_density.dat' u 11:28 ti ''     w l ls 41 lw 6 ,\
     '../../data/eta_density.dat' u 11:29 ti ''     w l ls 43,\
     '../../data/eta_density.dat' u 11:30 ti ''     w l ls 42      

set yl ""
set format y ""
set lmargin 0
set rmargin mar
plot '../../data/eta_barotropic.dat' u 11:14 ti 'e'    w l ls  9 lw 8,\
     '../../data/eta_barotropic.dat' u 11:15 ti ''     w l ls  2 ,\
     '../../data/eta_barotropic.dat' u 11:19 ti ''     w l ls  3 ,\
     '../../data/eta_barotropic.dat' u 11:18 ti ''     w l ls  4 ,\
     '../../data/eta_barotropic.dat' u 11:20 ti ''     w l ls  6 ,\
     '../../data/eta_barotropic.dat' u 11:21 ti ''     w l ls  7 ,\
     '../../data/eta_barotropic.dat' u 11:17 ti ''     w l ls  8 ,\
     '../../data/eta_barotropic.dat' u 11:24 ti ''     w l ls  9 ,\
     '../../data/eta_barotropic.dat' u 11:22 ti ''     w l ls 15 ,\
     '../../data/eta_barotropic.dat' u 11:23 ti ''     w l ls 16 ,\
     '../../data/eta_barotropic.dat' u 11:16 ti ''     w l ls 10 ,\
     '../../data/eta_barotropic.dat' u 11:25 ti 'H_2+' w l ls 12 ,\
     '../../data/eta_barotropic.dat' u 11:26 ti 'K+'   w l ls 13 ,\
     '../../data/eta_barotropic.dat' u 11:27 ti 'Na+'  w l ls 14 ,\
     '../../data/eta_barotropic.dat' u 11:28 ti 'G+'   w l ls 41 lw 6 ,\
     '../../data/eta_barotropic.dat' u 11:29 ti 'G0'   w l ls 43,\
     '../../data/eta_barotropic.dat' u 11:30 ti 'G-'   w l ls 42   


#Plot conductivities 
set tmargin 0
set lmargin mar
set rmargin 0
set log y 
set yl 'log n (cm^{-3})' 
set yr [1e-11:nmax] 
set format y ' %3L'
set key top left
set format x2 "" 
set x2l '' 
set log y 
set yl 'log {/Symbol s} (s^{-1})' 
set yr [5e-8:1e16] 
set key top right 
set format y ' %3L' 
plot '../../data/eta_density.dat' u 11:   7  ti '{/Symbol s}_O'                w l ls 41 lw 6,\
     '../../data/eta_density.dat' u 11:($8>0.0? $8:0/0) ti '{/Symbol s}_H > 0' w l ls 43 ,\
     '../../data/eta_density.dat' u 11:($8<0.0?-$8:0/0) ti '{/Symbol s}_H < 0' w l ls 44 ,\
     '../../data/eta_density.dat' u 11:   9  ti '{/Symbol s}_P'                w l ls 42
 
set yl ""
set format y ""
unset key
set lmargin 0
set rmargin mar
plot '../../data/eta_barotropic.dat' u 11:   7  ti '{/Symbol s}_O'                w l ls 41 lw 6,\
     '../../data/eta_barotropic.dat' u 11:($8>0.0? $8:0/0) ti '{/Symbol s}_H > 0' w l ls 43 ,\
     '../../data/eta_barotropic.dat' u 11:($8<0.0?-$8:0/0) ti '{/Symbol s}_H < 0' w l ls 44 ,\
     '../../data/eta_barotropic.dat' u 11:   9  ti '{/Symbol s}_P'                w l ls 42

set xl 'log n_n(cm^{-3})' 
set format x '{%L}'
#Plot resistivities 
set lmargin mar
set rmargin 0
set key bottom left 
set yl 'log {/Symbol h} (cm^2 s^{-1})' 
set format y ' %3L'
set yr [1e0:1e26] 
plot '../../data/eta_density.dat' u 11:   4  ti '{/Symbol h}_{OR}'                w l ls 41 lw 6,\
     '../../data/eta_density.dat' u 11:($5>0.0? $5:0/0) ti '{/Symbol h}_{HE} > 0' w l ls 43 ,\
     '../../data/eta_density.dat' u 11:($5<0.0?-$5:0/0) ti '{/Symbol h}_{HE} < 0' w l ls 44 ,\
     '../../data/eta_density.dat' u 11:   6  ti '{/Symbol h}_{AD}'                w l ls 42

set yl ""
set format y ""
unset key
set lmargin 0
set rmargin mar
plot '../../data/eta_barotropic.dat' u 11:   4  ti '{/Symbol h}_{OR}'                w l ls 41 lw 6,\
     '../../data/eta_barotropic.dat' u 11:($5>0.0? $5:0/0) ti '{/Symbol h}_{HE} > 0' w l ls 43 ,\
     '../../data/eta_barotropic.dat' u 11:($5<0.0?-$5:0/0) ti '{/Symbol h}_{HE} < 0' w l ls 44 ,\
     '../../data/eta_barotropic.dat' u 11:   6  ti '{/Symbol h}_{AD}'                w l ls 42

unset multiplot 
!open fig1a.eps