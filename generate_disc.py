#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#         Example programme to test various parameters of NICIL        !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
#+
# This is a short script that will generate the parameter file for
# the disc study, then run nicil.
#+
#----------------------------------------------------------------------!
import os
import sys
import time

# Definition to return a logical
def get_linput(Question,default):
   if (    default): cDefault = " [default=yes]: "
   if (not default): cDefault = " [default=no]: "
   cinput = raw_input(Question+cDefault)
   cinput = (cinput.strip()).lstrip()
   if (cinput[0:1]=="t" or cinput[0:1]=="T" or cinput[0:1]=="y" or cinput[0:1]=="Y"):
      linput = True
   elif (cinput[0:1]=="f" or cinput[0:1]=="F" or cinput[0:1]=="n" or cinput[0:1]=="N"):
      linput = False
   elif (cinput[0:1]=="q" or cinput[0:1]=="Q"):
      sys.exit()
   else:
      linput = default
   return linput

# Definition to return a real number
def get_rinput(Question,default):
   cinput = raw_input(Question+" [default="+str(default)+"]: ").strip()
   dloc = max(cinput.find("d"),cinput.find("D"))
   if (dloc > 0): cinput = cinput[0:dloc]+"e"+cinput[dloc+1:]
   try:
      isinstance(float(cinput),float)
      iinput = float(cinput)
   except:
     iinput = default
   return iinput

#----------------------------------------------------------------------!
# Ensure nicil_ex_eta has been compile
if (not os.path.isfile('nicil_ex_eta')):
   print "Compiling nicil"
   os.system('make eta')
   print " "
#----------------------------------------------------------------------!
# State what na is, and warn the user if they want to change it
a = open('src/nicil.F90','r')
for line in a:
   if ('integer, public, parameter :: na' in line):  break
a.close()
na = int(line[line.find('=')+1:line.find('!')])
print "Nicil will evolve na = "+str(na)+" grain sizes."
cont = raw_input("Press enter to continue, or q to quit so that you can recompile.")
if (cont[0:1]=="q" or cont[0:1]=="Q"): sys.exit()

#----------------------------------------------------------------------!
# Default inputs
disc_rin     =  1.00
disc_rout    = 40.00
disc_zmax    = 30.00
disc_mstar   =  0.80
disc_csin    =  2.00E+05
disc_rho0    =  1.00E-09
disc_B0      =  0.01
disc_Bindex  =  0.25
disc_gamma   =  1.00
disc_p       =  1.50
disc_q       =  0.25
disc_rhomin  =  1.00E-18
zeta_of_rho  =  'F'
use_fdg_in   =  'F'
a0_grain     =  1.00E-05
an_grain     =  1.00E-06
ax_grain     =  1.00E-01

# Prompt for user inputs; defaults are provided
print "The disc profile is given by:"
print ""
print "   rho = rho0 * (r/r_in)^{-p} exp(-0.5*z^2/H^2)"
print "where"
print "   H = c_s/Omega"
print "with"
print "   c_s = c_{s,in} * (r/r_in)^{-q}"
print "   Omega \propto sqrt(Mstar)"
print ""
disc_rin   = get_rinput('Enter the inner radius of the disc, r_in < au >',disc_rin)
disc_rout  = get_rinput('Enter the outer radius of the disc < au >',disc_rout)
disc_zmax  = get_rinput('Enter the maximum vertical extent of the disc to be modelled < au >',disc_zmax)
disc_mstar = get_rinput('Enter the mass of the central star < M_sun >',disc_mstar)
disc_csin  = get_rinput('Enter the sound speed at r_in < cm/s >',disc_csin)
disc_rho0  = get_rinput('Enter the density at r_in < g/cm^3 >',disc_rho0)
disc_p     = get_rinput('Enter the p-index required for rho',disc_p)
disc_q     = get_rinput('Enter the q-index required for c_s',disc_q)
print ""
print "The magnetic field strength is given by:"
print ""
print "   B = B0 * (rho/rho0)^Bindex"
print ""
disc_B0      = get_rinput('Enter B0, the magnetic field strength at rho0 < G >',disc_B0)
disc_Bindex  = get_rinput('Enter the Bindex', disc_Bindex)

zeta_of_rho_ask = get_linput('Use attenuated cosmic ray ionisation rate?', False)
if (zeta_of_rho_ask): zeta_of_rho = 'T'
if (na==10):
   use_fdg_in_ask = get_linput('Import dust-to-gas ratio from parent code?', False)
   if (use_fdg_in_ask): use_fdg_in = 'T'
if (na==1):
   a0_grain = get_rinput('Enter the grain radius of the single population < cm >',a0_grain)
else:
   an_grain = get_rinput('Enter the minimum grain radius of the grain distribution < cm >',an_grain)
   ax_grain = get_rinput('Enter the maximum grain radius of the grain distribution < cm >',ax_grain)
print ""

#----------------------------------------------------------------------!
# Get file suffix; request overwriting
prefix = 'disc_param_'
midfix = 'user000'
suffix = '.in'
i = 0
while (os.path.isfile(prefix+midfix+suffix)):
  i = i+1
  midfix = 'user'+str(i).rjust(3,'0')
overwrite = False
while (not overwrite):
   midfix_in = raw_input("Enter the parameter model name. [default = "+midfix+"]: ")
   if (midfix_in == "" or not os.path.isfile(prefix+midfix_in+suffix)):
      overwrite = True
   else:
      overwrite = get_linput("That file exists.  Do you wish to overwrite it?",False)
if (midfix_in <> ""): midfix = midfix_in
paramfile = prefix+midfix+suffix
print ""

#----------------------------------------------------------------------!
# Write the parameter file
a = open(paramfile,'w')
a.write('User generated parameter file.  Created '+time.strftime('%d').lstrip('0')+' '+time.strftime('%B')+' '+time.strftime('%Y')+'\n')
a.write('\n')
a.write('Disc parameters:\n')
a.write('disc_rin     = '+str(disc_rin   ).rjust(18,' ')+' ! inner disc radii [au]\n')
a.write('disc_rout    = '+str(disc_rout  ).rjust(18,' ')+' ! outer disc radii [au]\n')
a.write('disc_zmax    = '+str(disc_zmax  ).rjust(18,' ')+' ! maximum vertical disc extent [au]\n')
a.write('disc_mstar   = '+str(disc_mstar ).rjust(18,' ')+' ! mass of the central star [Msun]\n')
a.write('disc_csin    = '+str(disc_csin  ).rjust(18,' ')+' ! sound speed at R_in [cm/s]\n')
a.write('disc_rho0    = '+str(disc_rho0  ).rjust(18,' ')+' ! density at R_in [g/cm^3]\n')
a.write('disc_B0      = '+str(disc_B0    ).rjust(18,' ')+' ! magnetic field strength at R_in [G]\n')
a.write('disc_Bindex  = '+str(disc_Bindex).rjust(18,' ')+' ! proportionality slope on the magnetic field\n')
a.write('disc_gamma   = '+str(disc_gamma ).rjust(18,' ')+' ! adiabatic index\n')
a.write('disc_p       = '+str(disc_p     ).rjust(18,' ')+' ! p-index\n')
a.write('disc_q       = '+str(disc_q     ).rjust(18,' ')+' ! q-index\n')
a.write('disc_rhomin  = '+str(disc_rhomin).rjust(18,' ')+' ! will not calculate values if rho < disc_rhomin [g/cm^3]\n')
a.write('\n')
a.write('Cosmic ray attenuation:\n')
a.write('zeta_of_rho  = '+str(zeta_of_rho).rjust(18,' ')+' ! Use attenuated cosmic ray ionisation rate\n')
a.write('\n')
a.write('Dust Properties:\n')
a.write('use_fdg_in   = '+str(use_fdg_in ).rjust(18,' ')+' ! Import dust-to-gas ratio from parent code\n')
a.write('a0_grain     = '+str(a0_grain   ).rjust(18,' ')+' ! Grain size if na = 1 [cm]\n')
a.write('an_grain     = '+str(an_grain   ).rjust(18,' ')+' ! Minimum grain size if na > 1 [cm]\n')
a.write('ax_grain     = '+str(ax_grain   ).rjust(18,' ')+' ! Maximum grain size if na > 1 [cm]\n')
a.close()
#----------------------------------------------------------------------!
# Display resolution
a = open('src/nicil_ex_eta.F90','r')
for line in a:
   if (' integer, parameter  :: ndisc_r ' in line):
      rdisc = int(line[line.find('=')+1:line.find('!')])
   if (' integer, parameter  :: ndisc_z ' in line):
      zdisc = int(line[line.find('=')+1:line.find('!')])
      break
a.close()
dr = (disc_rout-disc_rin)/(rdisc-4)
dz = 2.0*disc_zmax/(zdisc-4)
print 'The non-ideal coefficients will be calculated with every dr = '+str(dr)+'au and dz = '+str(dz)+' au.'
print 'If this is not acceptable, please change ndisc_r & ndisc_z in src/nicil_ex_eta.F90 and recompile.'

#----------------------------------------------------------------------!
# Request if you want to run nicil now
runnow = get_linput('Run nicil now?',True)
if (runnow):
   print "Running nicil to generate disc"
   os.system('./nicil_ex_eta '+paramfile)
   # Prompt to make graph
   print "Running plotting script: recommended entries are 'enter' then '18'"
   os.system('python plot_results.py')
