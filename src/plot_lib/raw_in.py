#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#              A raw_input wrapper for compatibility with              !
#                        Python 2.7 & python 3.                        !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import sys
def raw_input_wrapper(string):
  if (sys.version_info[0] < 3):
     raw_input_wrapper = raw_input(string)
  else:
     raw_input_wrapper = input(string)
  return raw_input_wrapper

