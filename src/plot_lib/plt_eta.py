#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#          Supplementary subroutines that make specific plots          !
#                 Plots: properties from nicil_ex_eta                  !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
#----------------------------------------------------------------------!
#--Determine the number of grains to plot
def get_na(datafile):
  b = open(datafile,'r')
  line = b.readline()
  b.close()
  na = 0
  while (line.find('n_G0') > 0):
    na = na + 1
    line = line[line.find('n_G0')+4:]
  print("There are "+str(na)+" grain populations")
  return na
#----------------------------------------------------------------------!
def eta_properties(a,datafile,graphfile,xtype,gnuplot5,setxr,setyr,plotrhon,incl_H12,na,graphfiles):
  print("Plotting < "+graphfile+" > from file < "+datafile+" >.")
  graphfiles = graphfiles+" "+graphfile
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  if (gnuplot5):
    for i in range( 1,10): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i   )+"\n")
    for i in range(10,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i+2 )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 dt 1            \n")
  else:
    for i in range( 1,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 lt "+str(i   )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 lt 1            \n")
  a.write("  \n")
  a.write("m = 2.310*1.6726219e-24 \n")
  a.write("set terminal postscript eps enhanced colour size 15,5 font 'Times-Roman,20' \n")
  a.write("set output '"+graphfile+"' \n")
  a.write("  \n")
  a.write("set multiplot layout 1,3 \n")
  a.write("set log x \n")
  # properties vs density
  if (xtype == 1):
    if (plotrhon):
      a.write("set xl  'log n_n(cm^{-3})' \n")
      if (setxr):
        a.write("set xr  [1e2  :1e22  ] \n")
        a.write("set x2r [1e2*m:1e22*m] \n")
        a.write("set x2l 'log {/Symbol r}_n (g cm^{-3})' \n")
        a.write("set log x2 \n")
        a.write("set format x2 '%L' \n")
        a.write("set x2tics \n")
        a.write("set xtics nomirror \n")
        xmin = "1.0e2"
        x = "11"
    else:
      a.write("set xl  'log n(cm^{-3})' \n")
      if (setxr):
        a.write("set xr [1e2*m:1e22*m] \n")
        a.write("set xl 'log {/Symbol r} (g cm^{-3})' \n")
        xmin = "1.0e2*m"
        x = "1"
  # properties vs temperature
  elif(xtype==2):
    if (setxr): a.write("set xr [1e1:1e5] \n")
    a.write("set xl 'log T (K)' \n")
    xmin = "1.0e1"
    x = "2"
  # properties vs zeta
  elif(xtype==3):
    if (setxr): a.write("set xr [1e-32:1e-8] \n")
    a.write("set xl 'log {/Symbol z} (s^{-1})' \n")
    xmin = "1.0e-32"
    x = "31"
  a.write("set format x '%L' \n")
  a.write("set key top left \n")
  a.write(" \n")
  a.write("#Plot number densities \n")
  a.write("set log y \n")
  a.write("set yl 'log n (cm^{-3})' \n")
  if (setyr):
     if (xtype==3):
        a.write("set yr [1e-18:1e6] \n")
     else:
        a.write("set yr [1e-11:1e24] \n")
  a.write("set format y ' %3L' \n")
  a.write("plot '"+datafile+"' u "+x+":14 ti 'e'       w l ls  9 lw 8,\\\n")
  a.write("     '"+datafile+"' u "+x+":15 ti 'H_3+'    w l ls  2 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":19 ti 'He+'     w l ls  3 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":18 ti 'H+'      w l ls  4 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":20 ti 'C+'      w l ls  6 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":21 ti 'O+'      w l ls  7 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":17 ti 'O_2+'    w l ls  8 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":24 ti 'Mg+'     w l ls  9 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":22 ti 'Si+'     w l ls 15 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":23 ti 'S+'      w l ls 16 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":16 ti 'HCO+'    w l ls 10 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":27 ti 'Na+'     w l ls 11 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":25 ti 'H_2+'    w l ls 12 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":26 ti 'K+'      w l ls 13 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":27 ti 'Na+'     w l ls 14 ,\\\n")
  gl = []
  gl.append('G+')
  gl.append('G0')
  gl.append('G-')
  for j in range(0,na):
     a.write("  '"+datafile+"' u "+x+":"+str(28+3*j)+" ti '"+gl[0]+"'  w l ls 41 lw 6 ,\\\n")
     a.write("  '"+datafile+"' u "+x+":"+str(29+3*j)+" ti '"+gl[1]+"'  w l ls 43,\\\n")
     a.write("  '"+datafile+"' u "+x+":"+str(30+3*j)+" ti '"+gl[2]+"'  w l ls 42 ,\\\n")
     gl[0] = ""
     gl[1] = ""
     gl[2] = ""
  if (incl_H12):
    a.write("   '"+datafile+"' u "+x+":12 ti 'H_2'     w l ls  5 ,\\\n")
    a.write("   '"+datafile+"' u "+x+":13 ti 'H'       w l ls 14 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1) ti ''  w d          \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot conductivities \n")
  a.write("set log y \n")
  a.write("set yl 'log {/Symbol s} (s^{-1})' \n")
  if (setyr):
    if (xtype==3):
      a.write("set yr [1e-15:1e20] \n")
    else:
      a.write("set yr [5e-8:1e22] \n")
  a.write("set key top left \n")
  a.write("set format y ' %3L' \n")
  a.write("plot '"+datafile+"' u "+x+":   7  ti '{/Symbol s}_O'                w l ls 41 lw 6,\\\n")
  a.write("     '"+datafile+"' u "+x+":($8>0.0? $8:0/0) ti '{/Symbol s}_H > 0' w l ls 43 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":($8<0.0?-$8:0/0) ti '{/Symbol s}_H < 0' w l ls 44 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":   9  ti '{/Symbol s}_P'                w l ls 42 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1) ti ''               w d         \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot resistivities \n")
  a.write("set key top left \n")
  a.write("set yl 'log {/Symbol h} (cm^2 s^{-1})' \n")
  if (setyr):
    if (xtype==3):
      a.write("set yr [1e6:1e25] \n")
    else:
      a.write("set yr [1e0:1e24] \n")
  a.write("plot '"+datafile+"' u "+x+":   4  ti '{/Symbol h}_{OR}'                w l ls 41 lw 6,\\\n")
  a.write("     '"+datafile+"' u "+x+":($5>0.0? $5:0/0) ti '{/Symbol h}_{HE} > 0' w l ls 43 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":($5<0.0?-$5:0/0) ti '{/Symbol h}_{HE} < 0' w l ls 44 ,\\\n")
  a.write("     '"+datafile+"' u "+x+":   6  ti '{/Symbol h}_{AD}'                w l ls 42 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1) ti ''                  w d         \n") # To avoid crashing if no data
  a.write("unset multiplot \n")
  return graphfiles