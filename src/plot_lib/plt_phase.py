#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#          Supplementary subroutines that make specific plots          !
#                         Plots: Phase spaces                          !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
#----------------------------------------------------------------------!
def phase_space(a,datafile,graphfile,phaseB,sftrack,gnuplot5,graphfiles):
  if (not os.path.isfile(datafile)): return
  print("Plotting < "+graphfile+" > from file < "+datafile+" >.")
  graphfiles = graphfiles+" "+graphfile
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  if (gnuplot5):
    a.write("set style line  1 lc 1 lw  3 lt 7 ps 2 \n")
    a.write("set style line  2 lc 2 lw  3 lt 7 ps 2 \n")
    a.write("set style line  3 lc 3 lw  3 lt 7 ps 2 \n")
    a.write("set style line  5 lc 9 lw  3 lt 7 ps 2 \n")
    a.write("set style line  4 lc 5 lw 20 lt 1      \n")
  else:
    a.write("set style line  1 lc 1 lw  3 lt 7 ps 2 \n")
    a.write("set style line  2 lc 2 lw  3 lt 7 ps 2 \n")
    a.write("set style line  3 lc 3 lw  3 lt 7 ps 2 \n")
    a.write("set style line  5 lc 5 lw  3 lt 7 ps 2 \n")
    a.write("set style line  4 lc 5 lw 20 lt 1      \n")
  a.write("set terminal postscript eps enhanced colour size 7,5 font 'Times-Roman,35'\n")
  a.write("set output '"+graphfile+"' \n")
  if (phaseB):
    a.write("set yr [1e-7:1e5] \n")
    a.write("set yl 'log(B) [G]' \n")
    yval = "2"
    if (sftrack):
      a.write("B0    = 1.146e-7\n")
      a.write("nA    = 1.e-22;    gamA  = 0.65\n")
      a.write("inB   =  4.29e-15; inC   = 2.88e-13; inD   = 5.21e-10;  inE   =  9.87e-10\n")
      a.write("igamB = -0.42;     igamC = 0.5;      igamD = 0.2;       igamE = -0.32\n")
      a.write("nnB   =  2.67e-15; nnC   = 4.43e-13; nnD   =  1.69e-12; nnE   =  6.0e-10; nnF   = 3.45e-09\n")
      a.write("ngamB = -0.42;     ngamC = 0.50;     ngamD = -0.25;     ngamE = -1.0;     ngamF =  1.1\n")
  else:
    a.write("set yr [10:2e5] \n")
    a.write("set yl 'log(T) [K]' \n")
    yval = "3"
    if (sftrack):
      a.write("T0     = 14.0;   mump1  = 1.0/(2.307*1.67262158e-24)\n")
      a.write("nA     = 2.0e10; nB     =  2.5e14; nC     = 1.0e20\n")
      a.write("gammaA = 0.5;    gammaB = -0.4;    gammaC = 0.37\n")
      a.write("set yr [10:2e5] \n")
  a.write("set xr [1e-22:1] \n")
  a.write("set log xy  \n")
  a.write("set xl 'log({/Symbol r}) [g cm^{-3}]' \n")
  a.write("set format x '%L' \n")
  a.write("set format y '%L' \n")
  a.write("set key above\n")
  a.write("plot '"+datafile+"' u 1:($7== 1?$"+yval+":0/0) ls 1 ti ' {/Symbol h}_O > {/Symbol h}_{A}, |{/Symbol h}_{H}|',    ")
  a.write("     '"+datafile+"' u 1:($7== 2?$"+yval+":0/0) ls 2 ti ' {/Symbol h}_H > {/Symbol h}_{A}, {/Symbol h}_{O}, -{/Symbol h}_{H}',")
  a.write("     '"+datafile+"' u 1:($7==-2?$"+yval+":0/0) ls 5 ti '-{/Symbol h}_H > {/Symbol h}_{A}, {/Symbol h}_{O},  {/Symbol h}_{H}',")
  a.write("     '"+datafile+"' u 1:($7== 3?$"+yval+":0/0) ls 3 ti ' {/Symbol h}_A > {/Symbol h}_{O}, |{/Symbol h}_{H}|',  ")
  if (sftrack):
    if (phaseB):
      a.write("B0 * (x/nA)**gamA * (1.0 + (x/inB))**igamB * (1.0 + (x/inC))**igamC * (1.0 + (x/inD))**igamD * (1.0 + (x/inE))**igamE w l ls 4 ti '',")
      a.write("B0 * (x/nA)**gamA * (1.0 + (x/nnB))**ngamB * (1.0 + (x/nnC))**ngamC * (1.0 + (x/nnD))**ngamD * (1.0 + (x/nnE))**ngamE* (1.0 + (x/nnF))**ngamF w l ls 4 ti ''\n")
    else:
      a.write("T0 * sqrt( 1.0 + (x*mump1/nA)**(2.0*gammaA)) * ( 1.0 + (x*mump1/nB) )**gammaB * ( 1.0 + (x*mump1/nC) )**gammaC w l ls 4 ti ''\n")
  else:
    a.write("\n")
  return graphfiles
#----------------------------------------------------------------------!
#--Plots 1D track of B and T
def BT_tracks(a,datafile,graphfile,graphfiles,gnuplot5):
  if (not os.path.isfile(datafile)): return
  print("Plotting < "+graphfile+" > from file < "+datafile+" >.")
  graphfiles = graphfiles+" "+graphfile
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  if (gnuplot5):
    a.write("set style line  1 lc 1 lw 3 ps 2 \n")
    a.write("set style line  3 lc 3 lw 3 ps 2 \n")
  else:
    a.write("set style line 1 lt 1 lc 1 lw 3 ps 2 \n")
    a.write("set style line 3 lt 1 lc 3 lw 3 ps 2 \n")
  a.write("  \n")
  a.write("m = 2.310*1.6726219e-24 \n")
  a.write("set terminal postscript eps enhanced colour size 10,5 font 'Times-Roman,20' \n")
  a.write("set output '"+graphfile+"' \n")
  a.write("  \n")
  a.write("set multiplot layout 1,2 \n")
  a.write("unset key \n")
  a.write("set log xy \n")
  a.write("set xr [1e2*m:1e22*m] \n")
  a.write("set xl 'log {/Symbol r} (g cm^{-3})' \n")
  a.write("set format x ' %3L' \n")
  a.write("#Plot Magnetic field \n")
  a.write("set yl 'log B (G)' \n")
  a.write("set yr [1e-8:1e6] \n")
  a.write("set format y ' %3L' \n")
  a.write("plot '"+datafile+"' u 1:3 w l ls 1 \n")
  a.write("#Plot Temperature \n")
  a.write("set yl 'log T (K)' \n")
  a.write("set yr [9:1e5] \n")
  a.write("set format y ' %3L' \n")
  a.write("plot '"+datafile+"' u 1:2 w l ls 1 \n")
  a.write("unset multiplot \n")
  return graphfiles