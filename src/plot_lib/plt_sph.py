#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#          Supplementary subroutines that make specific plots          !
#                           Plots: SPH data                            !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
#----------------------------------------------------------------------!
def sph_data(a,datafile,graphfile,graphfiles,gnuplot5,setxr,setyr):
  if (not os.path.isfile(datafile)): return
  print("Plotting < "+graphfile+" > from file < "+datafile+" >.")
  graphfiles = graphfiles+" "+graphfile
  xmin   = "1.8"
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  if (gnuplot5):
    for i in range( 1,10): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i   )+"\n")
    for i in range(10,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i+2 )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 dt 1            \n")
  else:
    for i in range( 1,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 lt "+str(i   )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 lt 1            \n")
  a.write("  \n")
  a.write("m = 2.310*1.6726219e-24 \n")
  a.write("set terminal postscript eps enhanced colour size 14,5 font 'Times-Roman,20' \n")
  a.write("set output '"+graphfile+"' \n")
  a.write("  \n")
  a.write("set multiplot layout 2,3 \n")
  if (setxr): a.write("set xr ["+xmin+":15.5] \n")
  a.write("set xl '{/Symbol r} (10^{-19} g cm^{-3})' \n")
  a.write(" \n")
  a.write("#Plot magnetic density \n")
  a.write("set yl 'J_y (10^{-52} G cm^{-1})' \n")
  if (setyr): a.write("set yr [-10:0] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):($3/1.0e-52) ti '' w l ls 9 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1)            ti '' w d         \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot du/dt_{non-ideal} \n")
  a.write("set yl 'dudt_{non-ideal} (erg g^{-1} s^{-1})' \n")
  a.write("set log y \n")
  a.write("set format y '10^{%3L}' \n")
  if (setyr): a.write("set yr [1e-7:1e-2] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):8 ti '' w l ls 9 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1) ti '' w d         \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot dB/dt_{non-ideal} \n")
  a.write("set yl 'dBdt_{non-ideal,z} (G s^{-1})' \n")
  if (setyr): a.write("set yr [1e-14:1e-11] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):4 ti '' w l ls 9 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1) ti '' w d         \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot resistivities \n")
  a.write("set log y \n")
  a.write("set yl '{/Symbol h} (cm^2 s^{-1})' \n")
  a.write("set key top left \n")
  if (setyr): a.write("set yr [1e8:1e22] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):5     ti '{/Symbol h}_{OR}'     w l ls 41 ,\\\n")
  a.write("     '"+datafile+"' u ($1/1.0e-19):6     ti '{/Symbol h}_{HE} > 0' w l ls 43 ,\\\n")
  a.write("     '"+datafile+"' u ($1/1.0e-19):(-$6) ti '{/Symbol h}_{HE} < 0' w l ls 44 ,\\\n")
  a.write("     '"+datafile+"' u ($1/1.0e-19):7     ti '{/Symbol h}_{AD}'     w l ls 42 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+"):(1)     ti ''                     w d          \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("#Plot Hall & ion drift velocities (2 plots) \n")
  a.write("unset log y \n")
  a.write("set format y '%g' \n")
  a.write("set yl 'v_{drift} (km s^{-1})' \n")
  a.write("set key top left \n")
  if (setyr): a.write("set yr [-0.2:0.05] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):($10/1000.)  ti 'v_{hall,y} ' w l ls 44 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+")  :(1) ti ''            w d         \n") # To avoid crashing if no data
  a.write(" \n")
  if (setyr): a.write("set yr [-9:0] \n")
  a.write("plot '"+datafile+"' u ($1/1.0e-19):( $9/1000.) ti 'v_{ion,x}'   w l ls 42 ,\\\n")
  a.write("     '"+datafile+"' u ("+xmin+")  :(1) ti ''            w d         \n") # To avoid crashing if no data
  a.write(" \n")
  a.write("unset multiplot \n")
  return graphfiles