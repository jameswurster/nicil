#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#          Supplementary subroutines that make specific plots          !
#                          Plots: Interactive                          !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
#----------------------------------------------------------------------!
#--Reset the script and define line styles for the interactive plots
def write_header(a):
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  a.write("set style line  1 lc 1 lt 7 ps 2 \n")
  a.write("set style line  2 lc 2 lt 7 ps 2 \n")
  a.write("set style line  3 lc 3 lt 7 ps 2 \n")
  a.write("set style line  5 lc 9 lt 7 ps 2 \n")
  a.write("set style line  7 lc 7 lt 5 ps 2 \n")
  a.write("set style line  8 lc 8 lt 5 ps 2 \n")
  return
#----------------------------------------------------------------------!
#--Plots eta vs rho & (T or B)
def interactive_eta(a,datafile,phaseB):
  if (not os.path.isfile(datafile)): return
  print("Interactively plotting from file < "+datafile+" >.")
  write_header(a)
  if (phaseB):
     a.write("set yl 'log(B) [G]' \n")
     a.write("set yr [1e-7:1e5] \n")
     yval = "2"
  else:
     a.write("set yl 'log(T) [K]' \n")
     a.write("set yr [10:2e5] \n")
     yval = "3"
  a.write("set xr [1e-22:1] \n")
  a.write("set log xyz  \n")
  a.write("set ticslevel 0 \n")
  a.write("set xl 'log({/Symbol r}) [g cm^{-3}]' \n")
  a.write("set format x '%L' \n")
  a.write("set format y '%L' \n")
  a.write("set format z '%L' \n")
  a.write("set key above \n")
  a.write("set zl 'log({/Symbol h}_{OR}) [cm^2 s^{-1}]' \n")
  a.write("splot '"+datafile+"' u 1:"+yval+":4 ls 1 ti ''\n")
  a.write("pause -1 \n")
  a.write("set zl 'log({/Symbol h}_{HE}) [cm^2 s^{-1}]' \n")
  a.write("splot '"+datafile+"' u 1:"+yval+":( $5) ls 2 ti '+{/Symbol h}_{HE}',")
  a.write("      '"+datafile+"' u 1:"+yval+":(-$5) ls 5 ti '-{/Symbol h}_{HE}'\n")
  a.write("pause -1 \n")
  a.write("set zl 'log({/Symbol h}_{AD}) [cm^2 s^{-1}]' \n")
  a.write("splot '"+datafile+"' u 1:"+yval+":6 ls 3 ti ''\n")
  a.write("pause -1 \n")
  return
#----------------------------------------------------------------------!
#--Plots where the various eta's are dominant in the rho-B-T phase space
def interactive_phase(a,datafile):
  if (not os.path.isfile(datafile)): return
  print("Interactively plotting from file < "+datafile+" >.")
  write_header(a)
  a.write("set xr [1e-22:1] \n")
  a.write("set yr [1e-7:1e5] \n")
  a.write("set zr [10:2e5] \n")
  a.write("set log xyz  \n")
  a.write("set ticslevel 0 \n")
  a.write("set xl 'log({/Symbol r}) [g cm^{-3}]' \n")
  a.write("set yl 'log(B) [G]' \n")
  a.write("set zl 'log(T) [K]' \n")
  a.write("set format x '%L' \n")
  a.write("set format y '%L' \n")
  a.write("set format z '%L' \n")
  a.write("unset key \n")
  a.write("set title 'dominant {/Symbol h}_{OR}' \n")
  a.write("splot '"+datafile+"' u 1:2:($7== 1?$3:0/0) ls 1 ti ''\n")
  a.write("pause -1 \n")
  a.write("set title 'dominant {/Symbol h}_{HE} > 0' \n")
  a.write("splot '"+datafile+"' u 1:2:($7== 2?$3:0/0) ls 2 ti '+{/Symbol h}_{HE}'\n")
  a.write("pause -1 \n")
  a.write("set title 'dominant {/Symbol h}_{HE} < 0' \n")
  a.write("splot '"+datafile+"' u 1:2:($7==-2?$3:0/0) ls 5 ti '-{/Symbol h}_{HE}'\n")
  a.write("pause -1 \n")
  a.write("set title 'dominant {/Symbol h}_{AD}' \n")
  a.write("splot '"+datafile+"' u 1:2:($7== 3?$3:0/0) ls 3 ti ''\n")
  a.write("pause -1 \n")
  a.write("set title '{/Symbol h}_{HE} > 0' \n")
  a.write("splot '"+datafile+"' u 1:2:($5 > 0?$3:0/0) ls 2 ti '+{/Symbol h}_{HE}'\n")
  a.write("pause -1 \n")
  a.write("set title '{/Symbol h}_{HE} < 0' \n")
  a.write("splot '"+datafile+"' u 1:2:($5 < 0?$3:0/0) ls 5 ti '-{/Symbol h}_{HE}'\n")
  a.write("pause -1 \n")

#----------------------------------------------------------------------!
# Plots the number of times each initial guess is uses in the interactive ion density solver
def itry(a,gnuplot5):
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  if (gnuplot5):
    for i in range( 1,10): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i   )+"\n")
    for i in range(10,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 dt "+str(i+2 )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 dt 1            \n")
  else:
    for i in range( 1,20): a.write("set style line  "+str(i)+" lc "+str(i   )+" lw 3 ps 2 lt "+str(i   )+"\n")
    for i in range(41,45): a.write("set style line  "+str(i)+" lc "+str(i-40)+" lw 3 ps 2 lt 1            \n")
  a.write("  \n")
  a.write("set xl 'Guess Number' \n")
  a.write("set yl 'number' \n")
  a.write("plot ")
  i = 0
  for file in os.listdir('data/'):
     if (file[0:5]=='itry_'):
        i = i + 1
        a.write('"data/'+file+'" u 1:2 ti "'+file+'" w p ls '+str(i)+', ')
  a.write(" \n")
  a.write("pause -1 \n")
