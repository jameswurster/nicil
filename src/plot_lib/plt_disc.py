#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#          Supplementary subroutines that make specific plots          !
#                     Plots: protoplanetary discs                      !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
#----------------------------------------------------------------------!
#--Plot the disc properties for a single frame from nicil_ex_eta disc
def disc_properties(a,datafile,graphfile,setzr,graphfiles):
  if (not os.path.isfile(datafile)): return
  graphfiles = graphfiles+" "+graphfile
  print("Plotting < "+graphfile+" > from file < "+datafile+" >.")
  a.write("reset \n")
  a.write("# Manually modifying the style types \n")
  a.write("set style line  1 lc 1 lt 7 ps 1 \n")
  a.write("set style line  2 lc 2 lt 7 ps 1 \n")
  a.write("set style line  3 lc 3 lt 7 ps 1 \n")
  a.write("set style line  5 lc 9 lt 7 ps 1 \n")
  a.write("set terminal postscript eps enhanced colour size 16,9 font 'Times-Roman,25'\n")
  a.write("set output '"+graphfile+"' \n")
  a.write("rhomin = 1.e-15 \n")
  a.write("set multiplot layout 3,4\n")
  a.write("set key above \n")
  a.write("set xl '' \n")
  a.write("set yl '' \n")
  a.write("plot '"+datafile+"' u ($1>rhomin?$8:0/0):($7== 3?$9:0/0) ls 3 ti ' {/Symbol h}_A > {/Symbol h}_{O}, |{/Symbol h}_{H}|',")
  a.write("     '"+datafile+"' u ($1>rhomin?$8:0/0):($7== 2?$9:0/0) ls 2 ti ' {/Symbol h}_H > {/Symbol h}_{A}, {/Symbol h}_{O}, -{/Symbol h}_{H}',")
  a.write("     '"+datafile+"' u ($1>rhomin?$8:0/0):($7==-2?$9:0/0) ls 5 ti '-{/Symbol h}_H > {/Symbol h}_{A}, {/Symbol h}_{O},  {/Symbol h}_{H}',")
  a.write("     '"+datafile+"' u ($1>rhomin?$8:0/0):($7== 1?$9:0/0) ls 1 ti ' {/Symbol h}_O > {/Symbol h}_{A}, |{/Symbol h}_{H}|'\n")
  a.write("unset key \n")
  a.write("set log z \n")
  a.write("set log cb \n")
  a.write("set pm3d \n")
  a.write("unset surf \n")
  a.write("set ticslevel 0 \n")
  a.write("set view 0,359.999\n") 
  a.write("set zl '' \n")
  if (setzr):
    a.write("set cbr [rhomin:1e-9] \n")
    a.write("set zr  [rhomin:1e-9] \n")
  a.write("set format cb '%L' \n")
  a.write("set title 'log (density) [g/cm^3]' ; splot '"+datafile+"' u 8:9:($1>rhomin? $1:0/0) ls 1\n")
  if (setzr):
    a.write("set cbr [1e-4:1e-1] \n")
    a.write("set zr  [1e-4:1e-1] \n")
  a.write("set title 'log (B-field) [G]' ;      splot '"+datafile+"' u 8:9:($1>rhomin? $2:0/0) ls 1\n")
  if (setzr):
    a.write("set cbr [100:1000] \n")
    a.write("set zr  [100:1000] \n")
  a.write("set title 'log (temperature) [K]' ;  splot '"+datafile+"' u 8:9:($1>rhomin? $3:0/0) ls 1\n")
  if (setzr):
    a.write("set cbr [1e8:1e22] \n")
    a.write("set zr  [1e8:1e22] \n")
  a.write("set title 'log ({/Symbol h}_{OR}) [cm^2/s]'     ; splot '"+datafile+"' u 8:9:($1>rhomin? $4:0/0) ls 1\n")
  a.write("set title 'log ({/Symbol h}_{HE} > 0) [cm^2/s]' ; splot '"+datafile+"' u 8:9:($1>rhomin? $5:0/0) ls 1\n")
  a.write("set title 'log ({/Symbol h}_{HE} < 0) [cm^2/s]' ; splot '"+datafile+"' u 8:9:($1>rhomin?-$5:0/0) ls 1\n")
  a.write("set title 'log ({/Symbol h}_{AD}) [cm^2/s]'     ; splot '"+datafile+"' u 8:9:($1>rhomin? $6:0/0) ls 1\n")
  if (setzr):
    a.write("set cbr [1.e-4:1.e4] \n")
    a.write("set zr  [1.e-4:1.e4] \n")
  a.write("set title 'log [{/Symbol h}_{OR}/{/Symbol h}_{AD}]' ;       splot '"+datafile+"' u 8:9:($1>rhomin? $4/$6:0/0) ls 1\n")
  a.write("set title 'log [({/Symbol h}_{HE} > 0)/{/Symbol h}_{AD}]' ; splot '"+datafile+"' u 8:9:($1>rhomin? $5/$6:0/0) ls 1\n")
  a.write("set title 'log [({/Symbol h}_{HE} < 0)/{/Symbol h}_{AD}]' ; splot '"+datafile+"' u 8:9:($1>rhomin?-$5/$6:0/0) ls 1\n")
  a.write("unset multiplot \n")
  return graphfiles
#----------------------------------------------------------------------!
#--Plot the disc frames for the evolution of the disc properties
def evol_disc_properties(a,dfilei):
  a.write('\n') 
  a.write('set xr [0:30]\n') 
  a.write('set yr [-25:25]\n') 
  a.write('set multiplot layout 2,5\n') 
  a.write('unset cbr\n')
  a.write('unset surf\n')
  a.write('unset pm3d\n')
  a.write('unset title\n')
  a.write('set key above\n') 
  a.write('set xl ""\n') 
  a.write('set yl ""\n')
  a.write('plot "'+dfilei+'" u ($1>rhomin?$2:0/0):($28== 3?$3:0/0) ls 3 ti "Ambipolar",') 
  a.write('     "'+dfilei+'" u ($1>rhomin?$2:0/0):($28== 2?$3:0/0) ls 2 ti "Hall > 0",') 
  a.write('     "'+dfilei+'" u ($1>rhomin?$2:0/0):($28==-2?$3:0/0) ls 5 ti "Hall < 0",') 
  a.write('     "'+dfilei+'" u ($1>rhomin?$2:0/0):($28== 1?$3:0/0) ls 1 ti "Ohmic"\n') 
  a.write('\n') 
  a.write('unset key\n') 
  a.write('set log z\n') 
  a.write('set log cb\n') 
  a.write('set pm3d\n') 
  a.write('unset surf\n') 
  a.write('set ticslevel 0\n') 
  a.write('set view 0,359.999\n') 
  a.write('set zl ""\n') 
  a.write('set format z ""\n') 
  a.write('set title "log (density) [g/cm^3]"\n') 
  a.write('set cbr [rhomin:1e-8]\n') 
  a.write('set zr  [rhomin:1e-8]\n') 
  a.write('set format cb "%L"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$1:0/0) ls 1\n') 
  a.write('\n') 
  a.write('set cbr [100:1000]\n') 
  a.write('set zr  [100:1000]\n') 
  a.write('set title "log (temperature) [K] "\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$4:0/0) ls 1\n') 
  a.write('\n') 
  a.write('\n') 
  a.write('set cbr [1e-4:1e0]\n') 
  a.write('set zr  [1e-4:1e0]\n') 
  a.write('set title "log (B-field) [G]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?sqrt($8**2+$9**2+$10**2):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set cbr [1e-20:1e0]\n') 
  a.write('set zr  [1e-20:1e0]\n') 
  a.write('set title "log |divB| [G/cm]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?abs($11):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set cbr [1e12:1e22]\n') 
  a.write('set zr  [1e12:1e22]\n') 
  a.write('set title "log (eta_{Ohm}) [cm^2/s]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$25:0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log (eta_{Hall} > 0) [cm^2/s]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$26:0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log (eta_{Hall} < 0) [cm^2/s]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?-$26:0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log(eta_{Ambi}) [cm^2/s]"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$27:0/0) ls 1\n') 
  a.write('\n')
  a.write('set cbr [1e-10:1e2]\n') 
  a.write('set zr  [1e-10:1e2]\n') 
  a.write('set title "log dx*|divB|/B"\n') 
  a.write('splot "'+dfilei+'" u 2:3:($1>rhomin?$12:0/0) ls 1\n') 
  a.write('\n') 
  a.write('unset multiplot\n') 
  a.write('\n') 
  return
#----------------------------------------------------------------------!
#--Plot the disc frames for the evolution of the magnetic field components
def evol_disc_B(a,dfilei):
  set_scale = False
  a.write('dBminplot = 1e-10\n') 
  a.write('\n') 
  a.write('set xr [0:30]\n') 
  a.write('set yr [-25:25]\n') 
  a.write('set multiplot layout 3,5\n') 
  a.write('Bmin  = 1.0e-4\n') 
  a.write('Bmax  = 1.0e0\n') 
  a.write('dBmin = 1.0e-4\n') 
  a.write('dBmax = 1.0e0\n') 
  a.write('unset key\n') 
  a.write('set log z\n') 
  a.write('set log cb\n') 
  a.write('set pm3d\n') 
  a.write('unset surf\n') 
  a.write('set ticslevel 0\n') 
  a.write('set view 0,359.9999\n') 
  a.write('set zl ""\n') 
  a.write('set format z ""\n')
  a.write('set format cb "%L"\n')
  if (set_scale):
     a.write('set cbr [Bmin:Bmax]\n') 
     a.write('set zr  [Bmin:Bmax]\n') 
  a.write('set title "log |B_x| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs( $8)>dBminplot?$3:0/0):($1>rhomin?abs( $8):0/0) ls 1\n') 
  a.write('\n')
  if (set_scale):
     a.write('set cbr [dBmin:dBmax]\n') 
     a.write('set zr  [dBmin:dBmax]\n') 
  a.write('set title "log |dB_x: Ideal| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($13)>dBminplot?$3:0/0):($1>rhomin?abs($13):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_x: Ohm| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($16)>dBminplot?$3:0/0):($1>rhomin?abs($16):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_x: Hall| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($19)>dBminplot?$3:0/0):($1>rhomin?abs($19):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_x: Ambi| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($22)>dBminplot?$3:0/0):($1>rhomin?abs($22):0/0) ls 1\n') 
  a.write('\n') 
  a.write('\n') 
  if (set_scale):
     a.write('set cbr [Bmin:Bmax]\n') 
     a.write('set zr  [Bmin:Bmax]\n') 
  a.write('set title "log |B_y| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs( $9)>dBminplot?$3:0/0):($1>rhomin?abs( $9):0/0) ls 1\n') 
  a.write('\n') 
  if (set_scale):
     a.write('set cbr [dBmin:dBmax]\n') 
     a.write('set zr  [dBmin:dBmax]\n') 
  a.write('set title "log |dB_y: Ideal| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($14)>dBminplot?$3:0/0):($1>rhomin?abs($14):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_y: Ohm| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($17)>dBminplot?$3:0/0):($1>rhomin?abs($17):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_y: Hall| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($20)>dBminplot?$3:0/0):($1>rhomin?abs($20):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_y: Ambi| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($23)>dBminplot?$3:0/0):($1>rhomin?abs($23):0/0) ls 1\n') 
  a.write('\n') 
  a.write('\n') 
  if (set_scale):
     a.write('set cbr [Bmin:Bmax]\n') 
     a.write('set zr  [Bmin:Bmax]\n') 
  a.write('set title "log |B_z| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($10)>dBminplot?$3:0/0):($1>rhomin?abs($10):0/0) ls 1\n') 
  a.write('\n') 
  if (set_scale):
     a.write('set cbr [dBmin:dBmax]\n') 
     a.write('set zr  [dBmin:dBmax]\n') 
  a.write('set title "log |dB_z: Ideal| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($15)>dBminplot?$3:0/0):($1>rhomin?abs($15):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_z: Ohm| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($18)>dBminplot?$3:0/0):($1>rhomin?abs($18):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_z: Hall| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($21)>dBminplot?$3:0/0):($1>rhomin?abs($21):0/0) ls 1\n') 
  a.write('\n') 
  a.write('set title "log |dB_z: Ambi| [G]"\n') 
  a.write('splot "'+dfilei+'" u (30):(25):(0.001), "'+dfilei+'" u 2:(abs($24)>dBminplot?$3:0/0):($1>rhomin?abs($24):0/0) ls 1\n') 
  a.write('\n') 
  a.write('\n') 
  a.write('unset multiplot\n') 
  a.write('\n') 
  return
#----------------------------------------------------------------------!
#--A wrapper to create the png images for the disc evolution movies
def evol_disc(a,datafileprefix,graphfileprefix,evol_B):
  i = 100
  j = 0
  dfilei = datafileprefix+str(i)+'.dat'
  a.write('reset\n') 
  a.write('set terminal png size 1920,1080\n')
  a.write('rhomin    = 1e-15\n') 
  while (os.path.isfile(dfilei)):
    a.write('print "Plotting data for '+dfilei+'"\n') 
    a.write('set output "'+graphfileprefix+str(j)+'.png"\n')
    if (evol_B):
      plt = evol_disc_B(a,dfilei)
    else:
      plt = evol_disc_properties(a,dfilei)
    i = i + 1
    j = j + 1
    dfilei = datafileprefix+str(i)+'.dat'
  return
