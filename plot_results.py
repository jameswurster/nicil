#----------------------------------------------------------------------!
#                               N I C I L                              !
#           Non-Ideal mhd Coefficient and Ionisation Library           !
#         A Control script to allow the user to generate graphs        !
#                         of the relevant data.                        !
#                                                                      !
#                 Copyright (c) 2015-2023 James Wurster                !
#        See LICENCE file for usage and distribution conditions        !
#----------------------------------------------------------------------!
import os
import sys
sys.path.insert(1,'src/plot_lib')
import raw_in
import plt_eta
import plt_sph
import plt_phase
import plt_interactive
import plt_disc

#--Defaults (these are useful values that may be temporarily altered for more useful graphing)
open_eps = "open"  # Program used to open the .eps files; leave blank to prevent auto-opening (e.g. open_eps="open")
all_menu = False   # Plot all menu items
setxr    = True    # Use the updated default x-range; else, gnuplot will automatically select the range
setyr    = True    # Use the updated default y-range; else, gnuplot will automatically select the range
setzr    = True    # Use the updated default z-range; else, gnuplot will automatically select the range
plotrhon = True    # Plot n_n & rho_n (else plot rho)
incl_H12 = False   # Include H2 on plots

#--Verify that Gnuplot exists.  If it does not, ask the user if they wish to contine
does_gnuplot_exist = os.system("gnuplot --version")
if (does_gnuplot_exist!=0):
  print(" ")
  print("Gnuplot does not exist on this computer")
  print("Would you like to continue and make the Gnuplot script?")
  print("This scipt can be used as a template to make the graphs in an exising graphing programme.")
  makescript = raw_in.raw_input_wrapper("Press Y then Enter to make the script; otherwise press Enter to exit: ")
  if (makescript!="Y" and makescript!="y"): sys.exit()

#--Determine if an old or new version of Gnuplot is being used, where 5.0 and newer is defined as new
gnuplot5    = True
bashcommand = "gnuplot --version"
line        = os.popen(bashcommand).read().strip()
if (" 0." in line or " 1." in line or " 2." in line or " 3." in line or " 4." in line): gnuplot5 = False

#--Print instructions if no default viewer is set
if (open_eps==""): 
  print(" ")
  print("!---------------------------------------------------------------------------------------------------!")
  print("! Graphs will not be automatically opened.                                                          !")
  print("! Please set 'open_eps' to the programme to which you wish to use to open the graphs automatically. !")
  print("!---------------------------------------------------------------------------------------------------!")
  print(" ")

#----------------------------------------------------------------------!
#--Main menu: What would you like to plot?
idflt  = 0
ival   = 0
opt    = []
dfile  = []
gfile  = []
dflt   = []
ival   = []
abv    = []
not_sph   = True
plotsuite = False
# the options
opt.append('properties vs. number density with T = constant');                           dfile.append('eta_density');         gfile.append('eta_density_Tcnst');               dflt.append(True); abv.append(True); ival.append( 1)
opt.append('properties vs. number density with T = T(n) {barotropic EOS}');              dfile.append('eta_barotropic');      gfile.append('eta_density_barotropic');          dflt.append(True); abv.append(True); ival.append( 2)
opt.append('properties vs. number density with T = T(n) {ideal MHD core collapse}');     dfile.append('eta_collapseIdeal');   gfile.append('eta_density_collapseIdeal');       dflt.append(True); abv.append(True); ival.append( 3)
opt.append('properties vs. number density with T = T(n) {non-ideal MHD core collapse}'); dfile.append('eta_collapseNonideal');gfile.append('eta_density_collapseNonideal');    dflt.append(True); abv.append(True); ival.append( 4)
opt.append('properties vs. temperature with n = constant');                              dfile.append('eta_temperature');     gfile.append('eta_temperature_ncnst');           dflt.append(True); abv.append(True); ival.append( 5)
opt.append('properties vs. temperature with n = n(T) {barotropic EOS}');                 dfile.append('eta_barotropic');      gfile.append('eta_temperature_barotropic');      dflt.append(True); abv.append(True); ival.append( 6)
opt.append('properties vs. temperature with n = n(T) {ideal MHD core collapse}');        dfile.append('eta_collapseIdeal');   gfile.append('eta_temperature_collapseIdeal');   dflt.append(True); abv.append(True); ival.append( 7)
opt.append('properties vs. temperature with n = n(T) {non-ideal MHD core collapse}');    dfile.append('eta_collapseNonideal');gfile.append('eta_temperature_collapseNonideal');dflt.append(True); abv.append(True); ival.append( 8)
opt.append('properties vs. zeta_cr');                                                    dfile.append('eta_zeta');            gfile.append('eta_zeta');                        dflt.append(True); abv.append(True); ival.append( 9)
opt.append('all of the above (assuming data exists)');                                   dfile.append('');                    gfile.append('');                                dflt.append(True); abv.append(False);ival.append(10)
opt.append('B and T for options all available data');                                    dfile.append('');                    gfile.append('');                                dflt.append(False);abv.append(False);ival.append(11)
opt.append('dominant eta: rho-B phase space diagram');                                   dfile.append('eta_phaseB');          gfile.append('eta_phaseB');                      dflt.append(True); abv.append(False);ival.append(12)
opt.append('dominant eta: rho-T phase space diagram');                                   dfile.append('eta_phaseT');          gfile.append('eta_phaseT');                      dflt.append(True); abv.append(False);ival.append(13)
opt.append('dominant eta: rho-T-B phase space diagram (3D interactive)');                dfile.append('eta_phase');           gfile.append('');                                dflt.append(True); abv.append(False);ival.append(14)
opt.append('eta from the rho-T phase space diagram (3D interactive)');                   dfile.append('eta_phaseB');          gfile.append('');                                dflt.append(False);abv.append(False);ival.append(16)
opt.append('eta from the rho-B phase space diagram (3D interactive)');                   dfile.append('eta_phaseT');          gfile.append('');                                dflt.append(False);abv.append(False);ival.append(17)
opt.append('all disc properties {from nicil_ex_eta disc}');                              dfile.append('eta_disc');            gfile.append('eta_disc_properties');             dflt.append(True); abv.append(False);ival.append(18)
opt.append('evolution of disc properties {from nicil_ex_disc}');                         dfile.append('disc_evol_100');       gfile.append('disc_evol_prop_');                 dflt.append(True); abv.append(False);ival.append(19)
opt.append('evolution of magnetic field contributions in the disc {from nicil_ex_disc}');dfile.append('disc_evol_100');       gfile.append('disc_evol_B_');                    dflt.append(False);abv.append(False);ival.append(20)
opt.append('properties vs. position for SPH test');                                      dfile.append('sph_density');         gfile.append('sph_density');                     dflt.append(True); abv.append(False);ival.append(21)
opt.append('number of initial guesses for the iterative ion-density solver');            dfile.append('');                    gfile.append('itry');                            dflt.append(True); abv.append(False);ival.append(22)

#--Get inputs
print(" ")
is_data = False
is_opt = []
print("What would you like to plot?  Given your current data, the options are")
for i in range(0,len(opt)):
  if (os.path.isfile('data/'+dfile[i]+'.dat') or all_menu or (dfile[i]=='' and plotsuite) or os.path.isfile('data/itry_*.dat')):
    print("  "+str(i+1).rjust(2,'0')+": "+opt[i])
    if ((dflt[i] and idflt==0) or ival[i]==10): idflt = i
    if (abv[i]): plotsuite = True
    if (ival[i]==21): not_sph = False
    is_data = True
    is_opt.append(i+1)
print(" ")
if (not is_data):
  print("Please generate data before plotting")
  print(" ")
  sys.exit()
keep_trying = True
while (keep_trying):
  cxaxis = raw_in.raw_input_wrapper("Enter the number for the desired axis [default="+str(idflt+1)+"]: ")
  if (cxaxis=="q" or cxaxis=="Q"): sys.exit()
  if (cxaxis==""):
    iplot = ival[idflt]
    keep_trying = False
  else:
    try:
      isinstance(int(cxaxis),int)
      iinput = int(cxaxis)
      for i in range(0,len(is_opt)):
         if (iinput==is_opt[i]):
            keep_trying = False
            iplot = ival[iinput-1]
            break
    except:
      keep_trying = True

sftrack = False
if (iplot==12 or iplot==13):
    psf = raw_in.raw_input_wrapper("Would you like to overplot the star formation tracks? [default = no]: ")
    if (psf=="q" or psf=="Q"): sys.exit()
    if (psf[0:1]=="y" or psf[0:1]=="Y" or psf[0:1]=="t" or psf[0:1]=="T"): sftrack = True

#----------------------------------------------------------------------!
#--Determine the plotting range
if (iplot == 10):
  imin = 1
  imax = 10
elif (iplot == 11):
  imin = 1
  imax = 5
elif (iplot >= 12):
  imin = 0
  imax = 0
else:
  imin = iplot
  imax = imin + 1
graphscript = "Graphs/plot.gp"
graphfiles  = " "
#----------------------------------------------------------------------!
#--Plot non-ideal coefficients and all relevant properties
a=open(graphscript,"w")
first_graph = True
for i in range(imin,imax):
  # Determine the data and graph file names
  for j in range(0,len(opt)):
    if (i == ival[j]):
       datafile  = 'data/'+dfile[j]+'.dat'
       graphfile = 'Graphs/'+gfile[j]+'.eps'
       break
  if (os.path.isfile(datafile)):
    if (first_graph):
      na = plt_eta.get_na(datafile)
      first_graph = False
    if (iplot==11):
      graphfiles = plt_phase.BT_tracks(a,datafile,graphfile,graphfiles,gnuplot5)
    else:
      xtype = 2
      if (i==9):
        xtype = 3
      elif (i <= 4):
        xtype = 1
      graphfiles = plt_eta.eta_properties(a,datafile,graphfile,xtype,gnuplot5,setxr,setyr,plotrhon,incl_H12,na,graphfiles)
#----------------------------------------------------------------------!
# Get data and graph names
for i in range(0,len(opt)):
  if (iplot == ival[i]):
     datafile  = 'data/'+dfile[i]+'.dat'
     graphfile = 'Graphs/'+gfile[i]+'.eps'
     break
#----------------------------------------------------------------------!
# Plot phase spaces
if (iplot==12):
   phaseB      = True
   graphfiles = plt_phase.phase_space(a,datafile,graphfile,phaseB,sftrack,gnuplot5,graphfiles)
elif (iplot==13):
   phaseB     = False
   graphfiles = plt_phase.phase_space(a,datafile,graphfile,phaseB,sftrack,gnuplot5,graphfiles)
#----------------------------------------------------------------------!
# Plot interactive eta
elif (iplot==14):
   plt    = plt_interactive.interactive_phase(a,datafile)
elif (iplot==16):
   phaseB = False
   plt    = plt_interactive.interactive_eta(a,datafile,phaseB)
elif (iplot==17):
   phaseB = True
   plt    = plt_interactive.interactive_eta(a,datafile,phaseB)
#----------------------------------------------------------------------!
# Plot discs
elif (iplot==18):
   is_movie   = False
   graphfiles = plt_disc.disc_properties(a,datafile,graphfile,setzr,graphfiles)
elif (iplot==19):
   evol_B     = False
   plt        = plt_disc.evol_disc(a,datafile[:-7],graphfile[:-4],evol_B)
elif (iplot==20):
   evol_B     = True
   plt        = plt_disc.evol_disc(a,datafile[:-7],graphfile[:-4],evol_B)
#----------------------------------------------------------------------!
# Plot SPH data
elif (iplot==21):
  graphfiles = plt_sph.sph_data(a,datafile,graphfile,graphfiles,gnuplot5,setxr,setyr)
#----------------------------------------------------------------------!
# Plot number of iterative guesses
elif (iplot==22):
  plt = plt_interactive.itry(a,gnuplot5)
#----------------------------------------------------------------------!
a.close()
#--Execute
print("Graph script made: "+graphscript)
if (does_gnuplot_exist==0): 
  os.system("gnuplot "+graphscript)
  if (graphfiles !=" " and open_eps!=""): os.system(open_eps+" "+graphfiles)
